---
title: Programme (TO BE UPDATED)
order: 3
---

## Programme Outline (TBD)

Date: October 11 or 12  


## Target audience

The intended audience includes anyone interested in moving faster into a future where semantic technology is the new traditional technology in the enterprise.  This may include:
* vendors and consultants, 
* researchers and practitioners from industry and government. 
* academic researchers interested in knowing what the state of applied practice is, perhaps because they wish to start a spin-off company, or because they wish to do research in areas where it is sorely needed.
* students interested in an industrial career after academia


## Previous editions of this workshop

[WaSABi 2013 @ ISWC 2013](http://2013.wasabi-ws.org)
[WaSABi 2014 @ ESWC 2014](http://2014.wasabi-ws.org)
[WaSABi 2015 @ ESWC 2015](http://2015.wasabi-ws.org)
