---
title: Submissions
order: 1
---

## Topics Of Interest

Papers should shed light on the issues and questions listed below.  We want you to share lessons learned from hard-won experience, not speculative ideas.  Describing applications is important, but not for its own sake. We will place greater value on papers that focus on describing what you learned from the application(s) you have built or worked with.  What you say about any of those applications should be concise and limited to what is necessary to describe the learning.  Say why what you learned is important and how it can be leveraged again under what circumstances. 



* Ontology applications: what was accomplished and what is the role of the semantic technology deployed?
* The role of the ontology in ontology applications. 
    - How complex are the ontologies? 
    - Do you maybe not even need one? Why or why not?
    - What role does inference play?
* Integrating enterprise with external linked data.
* Ontology engineering: describe solutions to challenging problems in specific domains that are likely to also arise in other domains.
* Triple Stores and SPARQL
*  Agile methodologies for development of semantic systems 
Model/Ontology Driven applications 
* Semantic systems and architectures for industrial challenge 
    - enterprise platforms using Semantic Web technology as part of the workflow
    - implementations and design patterns for enterprise systems
* The role of inference, when is SPARQL not enough? 
* How and when (if at all) should NoSQL and/or schema-less graph databases like Neo4j be used instead of or alongside RDF-based triple stores?
* What are strategies for engaging companies so that they are best able to exploit semantic technologies?
* Value Assessment of the semantic technology
    - what kind of value are you finding,
    - are there any qualitative or quantitative metrics, and how can you do the measuring?
    - which benefits are you seeing most often and why?
    - which benefits did you expect, but are not happening, why? 
* How to explain semantic technology to both technical and non-technical persons, including new architectures, complex ontologies etc.  May include visualization techniques.
* How to integrate informal knowledge assets such as terminologies, taxonomies and thesauri with more formal ontologies?
* How to integrate knowledge assets that are internal to a given organization with external assets?
* What kinds of tools are you finding most important in your work?
* What tool stacks have you found good or wanting for what purposes?
* Change management and governance for semantic systems and assets
* What is wrong with semantic technology today? What do you really need to move forward in building and maintaining enterprise solutions? 
* Comparative studies on the evolution of Semantic Web adoption
* Surveys looking into any of the above topics.


## Submissions

Submission criteria are as follows:

* [UPDATE] Papers must adhere to the [LNCS format guidelines](http://www.springer.com/computer/lncs?SGWID=0-164-6-793341-0).
* Papers are limited to eight pages (including figures, tables and appendices).
* [UPDATE] Papers are submitted in PDF format via [the workshop's EasyChair submission pages](https://easychair.org/conferences/?conf=wasabi2015). 

There will be a mix of short talks (10-15 min) and long talks (20-30 min) including Q&A.

## Important dates

Submission
: Wednesday July 1-15, 2015 

Notification
: Thursday July 30, 2015

Camera ready version
: Sunday August 16, 2015

Workshop
: October 11-12 (TBD), 2015

[UPDATE] For enquiries, please contact the organizers at [wasabi2015@easychair.org](mailto:wasabi2015@easychair.org)
