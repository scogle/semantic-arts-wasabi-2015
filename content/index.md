---
title: Introduction
order: 0
header: "Semantic Web Business: Lessons Learned from the Trenches"
---

Welcome to the WaSABi2015-West workshop homepage.

## The challenge
The WaSABi workshop series aims at helping to guide the conversation between the scientific research community, IT practitioners and industry. The workshops help to establish best practices for the development, deployment and evaluation of Semantic Web based technologies. Both research and industry communities can  greatly benefit from this discussion.  

The goal of this particular workshop is to have people who are working in the trenches in industry, government and education share their experiences with using semantic web technology. This will include sharing use cases, user stories, practical development issues, evaluations & design patterns. Semantic technology is still relatively new, and take-up is very small, compared to more traditional technologies. However, there is a lot of gathering momentum in the past few years. There are more and more deployments using semantic technologies. There are more and more people inside very traditional large enterprises that are familiar with and interested in semantic technology. Yet, there many pitfalls along the way and many things to learn. This workshop will be an important step along the slow steady path to making semantic technology the new “traditional” technology. We believe that the future of enterprise computing depends heavily on semantic technology.

## The solution
The gap between the Semantic Web research and industry practitioner communities, as evidenced by the limited uptake of very promising technologies in the broader market needs to be addressed. Researchers steadily improve upon modelling, query languages, reasoners, triple stores, and development tooling - but all too often, these improvements are driven not by real business needs, but intrinsically by the interests of researchers to work on and solve interesting challenges, or to obtain public funding. Conversely, practitioners are often unaware of how existing Semantic Web technologies already can help them to solve their problems (such as data or service integration, conflict detection and resolution, or data processing). Even in cases where they do know about these Semantic Web solutions, many practitioners lack the knowledge about tooling, scalability issues, design patterns that are required in order to successfully apply these technologies.

In order to help analyse and ultimately bridge this gap, the workshop organisers believe two things are needed: firstly, a greater understanding of industrial organisations and their needs, guiding them in selecting problems to work on that are of direct relevance to them, and secondly, to establish a set of methodologies, evaluation strategies and best practices for Semantic Web technology development and use, guiding practitioners who want to apply these technologies first hand. The WaSABi workshop provides a forum for discussing and developing solutions to both of these needs, neither of which can be solved by researchers or practitioners on their own.

In summary, The workshop aims to get people who are working in the trenches in industry, government and education to share their experiences that show a preview of  the future of enterprise computing through the use of semantic technology.  The full promise of semantic technology will only be realized when it is integrated into the enterprise architecture.  
