---
title: Committees (TO BE UPDATED)
order: 5
---

## Organizing committee

UPDATE emails

* [Michael Uschold](mailto:michael.uschold@semanticarts.com), Semantic Arts
* [Juan Sequada](mailto:foo@bar), Capsenta
* [Dean Allemang](mailto:foo@bar) Working Ontologist 

## Program committee
* Chris Welty, Google, USA
* Michael Gruninger, U. Toronto, Canada
* Joanne Luciano, RPI & Predictive Medicine, Inc., USA 
* Deborah McGuiness, RPI, USA
* Martin Hepp, Universität der Bundeswehr Munich, Germany
* Mark Greaves
* Alan Ruttenberg, University at Buffalo, The State University of New York, Buffalo, NY, (US)
* Dave McComb, Semantic Arts, USA
* Leo Obrst, Mitre (need new email)
* Marco Neumann, KONA, USA
* Mary Parmalee, Mitre, USA
* Brian Haugh, IDA, USA



